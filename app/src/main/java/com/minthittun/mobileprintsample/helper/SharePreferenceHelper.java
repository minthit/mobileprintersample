package com.minthittun.mobileprintsample.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePreferenceHelper {

	private SharedPreferences sharedPreference;

	private static String SHARE_PREFRENCE = "PrinterTest";

	private static String PRINTER_ADDRESS = "printer_address";
		
	public SharePreferenceHelper(Context context)
	{
		sharedPreference = context.getSharedPreferences(SHARE_PREFRENCE, Context.MODE_PRIVATE);
	}

	public void setPrinterAddress(String address)
	{
		SharedPreferences.Editor editor = sharedPreference.edit();
		editor.putString(PRINTER_ADDRESS, address);
		editor.commit();
	}

	public String getPrinterAddress()
	{
		return sharedPreference.getString(PRINTER_ADDRESS, "");
	}

	public boolean isPrinterSet()
	{
		if(sharedPreference.contains(PRINTER_ADDRESS))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
